//console.log("Hello World!");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock","Misty"]
	},
	
	talk: function(){
		console.log("Pikachu! I choose you!");
	}

}

console.log("Result of dot notation");
console.log(trainer.name);

console.log("Result of bracket notation");
console.log(trainer['pokemon']);

console.log("Result of talk method");
trainer.talk();





let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);


function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);

		if (target.health <= 0) {
			this.faint(target);
		}
	};
	this.faint = function(target) {
		console.log(target.name + " fainted");
	};
}
geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);
